import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("VERSION", "r") as fh:
    version = fh.read().strip()

setuptools.setup(
    name="hugeplots",
    version=version,
    author="Leberwurscht",
    author_email="leberwurscht@hoegners.de",
    description="Matplotlib plots that reexecute data-reducing preprocessors when zooming. Allows plotting of huge datasets.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/leberwurscht/hugeplots",
    packages=setuptools.find_packages(),
    install_requires=[
        'numpy',
        'matplotlib',
        'git+https://gitlab.com/leberwurscht/binreduce@v0.0.7',
        'git+https://gitlab.com/leberwurscht/pcolormesh_preprocess@v0.0.1'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3'
)
