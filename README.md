Matplotlib plots that reexecute data-reducing preprocessors when zooming. Allows plotting of huge datasets.

Installation:

```
pip3 install git+https://gitlab.com/leberwurscht/hugeplots.git
```

Example usage:

```python
import matplotlib.pyplot as plt
import numpy as np
import hugeplots

x = np.arange(5000)[:,None]
y = np.arange(5000)[None,:]
x = x - x.mean()
y = y - y.mean()
z = np.exp(-np.sin(x**2 + y**2)/5000**2)

plt.figure()
hugeplots.setup((x,y,z))
plt.show()
```
