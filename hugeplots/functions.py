import functools

import matplotlib.pyplot as plt
import numpy as np

import binreduce
from pcolormesh_preprocess import pcmp

def default_preprocessor(data,xlims=None,ylims=None, operation=binreduce.binmean):
  if xlims is None: xlims = data[0].min(), data[0].max()
  if ylims is None: ylims = data[1].min(), data[1].max()

  if len(data)==2:
    if data[0].size>15000:
      return binreduce.multi_binreduce(data[0], np.linspace(max(xlims[0],data[0].min()),min(xlims[1],data[0].max()),15000), data[1], operation=operation)
    else:
      return binreduce.multi_binreduce(data[0], None, data[1], operation=operation)
  elif len(data)==3:
    return binreduce.multi_binreduce(data[0], np.linspace(max(xlims[0],data[0].min()),min(xlims[1],data[0].max()),500), 
        data[1], np.linspace(max(ylims[0],data[1].min()),min(ylims[1],data[1].max()),500),
        data[2], operation=operation)
  else:
    raise NotImplementedError()

max_preprocessor = functools.partial(default_preprocessor, operation=binreduce.binmax)
min_preprocessor = functools.partial(default_preprocessor, operation=binreduce.binmin)

def default_plotfunc(data, *args, **kwargs):
  if len(data)==2:
    return plt.plot(*data, *args, **kwargs)
  elif len(data)==3:
    return [plt.pcolormesh(*pcmp(*data), *args, **kwargs)]
  else:
    raise NotImplementedError()

def setup(data, *plotfuncargs, plotfunc=default_plotfunc, preprocessor=default_preprocessor, fix_pos=None, **plotfunckwargs):
  # preprocess data and setup plot
  preprocessed_data = preprocessor(data)
  artists = plotfunc(preprocessed_data, *plotfuncargs, **plotfunckwargs)
  axes = artists[0].axes
  xlims = list(axes.get_xlim())
  ylims = list(axes.get_ylim())
  axes.autoscale(False)
  subaxes = []

  # callback to re-preprocess data and plot when limits are changed
  def lims_changed(*args):
    new_xlims = list(axes.get_xlim())
    new_ylims = list(axes.get_ylim())

    if fix_pos is not None:
      span_x = new_xlims[1]-new_xlims[0]
      span_y = new_ylims[1]-new_ylims[0]
      new_xlims[0] = fix_pos[0] - span_x/2
      new_xlims[1] = fix_pos[0] + span_x/2
      new_ylims[0] = fix_pos[1] - span_y/2
      new_ylims[1] = fix_pos[1] + span_y/2

    if (new_xlims,new_ylims)==(xlims,ylims): return

    axes.set_xlim(new_xlims, emit=False)
    axes.set_ylim(new_ylims, emit=False)

    del xlims[:], ylims[:]
    xlims.extend(new_xlims)
    ylims.extend(new_ylims)

    for a in artists: a.remove() # remove from plot
    del artists[:] # remove from list
    
    preprocessed_data = preprocessor(data, xlims, ylims)

    ax_before = plt.gca()
    plt.sca(axes)

    new_artists = plotfunc(preprocessed_data, *plotfuncargs, **plotfunckwargs)
    artists.extend(new_artists)
    axes.figure.canvas.draw()

    plt.sca(ax_before)

  # double-click callback: open zoom window with fixed center coordinates (fix_pos)
  def buttonpress(event):
    if not event.dblclick: return
    if not event.inaxes==axes: return

    if len(subaxes):
      sub, fixpos = subaxes[0]
      xl=sub.get_xlim()
      yl=sub.get_ylim()
      del fixpos[:]
      fixpos.extend([event.xdata,event.ydata])
      sub.set_xlim(xl)
      sub.set_ylim(yl)
    else:
      current_fig = plt.gcf()
      current_axes = plt.gca()

      subfig = plt.figure()
      fix_pos = [event.xdata,event.ydata]
      subaxes.append([plt.gca(), fix_pos])
      plt.title("zoom window")

      setup(data, *plotfuncargs, plotfunc=plotfunc, preprocessor=preprocessor, fix_pos=fix_pos, **plotfunckwargs)
      subfig.show()

      plt.figure(current_fig.number)
      plt.sca(current_axes)

  # register callbacks
  artists[0].axes.callbacks.connect("ylim_changed", lims_changed)
  artists[0].axes.callbacks.connect("xlim_changed", lims_changed)
  artists[0].axes.figure.canvas.mpl_connect("button_press_event", buttonpress)

  # trigger reset of center coordinates if fix_pos given
  if fix_pos is not None:
    axes.set_xlim(xlims)

  # TOOD: handle closing of zoom window

if __name__=="__main__":
  import matplotlib.pyplot as plt
  import numpy as np
  import binreduce

  x = np.arange(5000)[:,None]
  y = np.arange(5000)[None,:]
  x = x - x.mean()
  y = y - y.mean()
  z = np.exp(-np.sin(x**2 + y**2)/5000**2)

  plt.figure()
  setup((x,y,z))
  plt.show()
